package course.labs.activitylab

import android.os.Bundle
import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.TextView

class ActivityOne : Activity() {

    // lifecycle counts

    var createCounter: Int = 0;
    var startCounter: Int = 0;
    var resumeCounter: Int = 0;
    var pauseCounter: Int = 0;
    var stopCounter: Int = 0;
    var destroyCounter: Int = 0;
    var restartCounter: Int = 0;

    private lateinit var createText: TextView;
    private lateinit var startText: TextView;
    private lateinit var resumeText: TextView;
    private lateinit var pauseText: TextView;
    private lateinit var stopText: TextView;
    private lateinit var destroyText: TextView;
    private lateinit var restartText: TextView;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one)

        createText = findViewById(R.id.create) as TextView;
        startText = findViewById(R.id.start) as TextView;
        resumeText = findViewById(R.id.resume) as TextView;
        pauseText = findViewById(R.id.pause) as TextView;
        stopText = findViewById(R.id.stop) as TextView;
        destroyText = findViewById(R.id.destroy) as TextView;
        restartText = findViewById(R.id.restart) as TextView;

        //Log cat print out
        Log.i(TAG, "onCreate called")
        createCounter++;
        this.updateCounters()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_one, menu)
        return true
    }

    fun updateCounters() {
        createText.setText(resources.getText(R.string.onCreate).toString() + this.createCounter.toString())
        startText.setText(resources.getText(R.string.onStart).toString() + this.startCounter)
        resumeText.setText(resources.getText(R.string.onResume).toString() + this.resumeCounter)
        pauseText.setText(resources.getText(R.string.onPause).toString() + this.pauseCounter)
        stopText.setText(resources.getText(R.string.onStop).toString() + this.stopCounter)
        destroyText.setText(resources.getText(R.string.onDestroy).toString() + this.destroyCounter)
        restartText.setText(resources.getText(R.string.onRestart).toString() + this.restartCounter)
    }

    // lifecycle callback overrides

    public override fun onStart() {
        super.onStart()

        Log.i(TAG, "onStart called")
        startCounter++;
        this.updateCounters()
    }

    public override fun onResume() {
        super.onResume()

        Log.i(TAG, "onResume called")
        resumeCounter++;
        this.updateCounters()
    }

    public override fun onPause() {
        super.onPause()

        Log.i(TAG, "onPause called")
       pauseCounter++;
        this.updateCounters()
    }

    public override fun onStop() {
        super.onStop()

        Log.i(TAG, "onStop called")
       stopCounter++;
        this.updateCounters()
    }

    public override fun onDestroy() {
        super.onDestroy()

        Log.i(TAG, "onDestroy called")
        destroyCounter++;
        this.updateCounters()
    }

    public override fun onRestart() {
        super.onRestart()

        Log.i(TAG, "onRestart called")
       restartCounter++;
        this.updateCounters()
    }

    // Note:  if you want to use a resource as a string you must do the following
    //  getResources().getString(R.string.stringname)   returns a String.

    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        //TODO:  save state information with a collection of key-value pairs & save all  count variables
    }


    fun launchActivityTwo(view: View) {
        startActivity(Intent(this, ActivityTwo::class.java))
    }

    // singleton object in kotlin
    // companion object
    companion object {
        // string for logcat
        private val TAG = "Lab-ActivityOne"
    }


}

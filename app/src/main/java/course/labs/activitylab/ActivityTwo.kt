package course.labs.activitylab

import android.os.Bundle
import android.app.Activity
import android.util.Log

class ActivityTwo : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_two)

        //Log cat print out
        Log.i(TAG, "onCreate called")
        finish()
    }

    companion object {
        // string for logcat
        private val TAG = "Lab-ActivityTwo"
    }
}
